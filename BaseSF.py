def get_number(c):
    """
    Return the Base64 number of the character
    specified.
    """

    numbers = [str(i) for i in range(10)]
    lettersCap = [chr(i) for i in range(ord("A"), ord("Z") + 1)]
    lettersMin = [chr(i) for i in range(ord("a"), ord("z") + 1)]
    special = ["/", "+"]

    if c in lettersCap:
        return ord(c) - 65

    elif c in lettersMin:
        return ord(c) - 71

    elif c in numbers:
        return ord(c) + 4

    elif c in special:
        if c == "/":
            return 63

        elif c == "+":
            return 62

    else:
        return 0


def get_char(bin_data):
    """
    Return the character associated with the
    Base64 encoding.
    """

    data = int(bin_data, 2)

    returned_data = "+"

    if data in range(26):
        returned_data = chr(data + 65)

    elif data in range(26, 52):
        returned_data = chr(data + 71)

    elif data in range(52, 62):
        returned_data = chr(data - 4)

    elif data == 62:
        returned_data = "+"

    elif data == 63:
        returned_data = "/"

    return returned_data
