# -*- coding: utf8 -*-

"""
The MIT License (MIT)
Copyright (c) 2016 Cristóbal Liendo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
    OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
    OR OTHER DEALINGS IN THE SOFTWARE.
"""

import BaseSF


class TextCypher():
    def lsfr(self, tap_1, tap_2, seed, ticks):
        """
        Returns a pseudo random array of binary numbers.
        seed is an binary array
        """
        pseudo_random = []
        sd_len = len(seed)

        for i in xrange(ticks):
            seed.append(seed[sd_len - tap_1] ^ seed[sd_len - tap_2])
            seed.pop(0)

            pseudo_random.append(seed[-1])

        return pseudo_random

    def get_bin_array(self, text, base64=False):
        bin_text = []

        bits = 8
        if base64:
            bits = 6

        for letter in text:
            for i in xrange(bits - 1, -1, -1):
                if base64:
                    bin_number = (BaseSF.get_number(letter) >> i) & 1
                else:
                    bin_number = (ord(letter) >> i) & 1

                bin_text.append(bin_number)

        return bin_text

    def GetArrayEncoded(self, text, seed, base64=False):
        bin_text = self.get_bin_array(text, base64=base64)

        seed_len = len(seed)
        text_len = len(bin_text)

        pseudo_random = self.lsfr(seed_len, seed_len - 2, seed, text_len)

        # Only for debbuging
        # print bin_text
        # print pseudo_random

        d_cell = []

        i = 0
        for bin_number in pseudo_random:
            d_cell.append(bin_number ^ bin_text[i])

            i += 1

        return d_cell

    def GetStringEncoded(self, cell, base64=False):
        cypher_text = ""

        bits = 8
        if base64:
            bits = 6

        i = 0
        prev_index = 0
        for i in xrange(len(cell) / bits):
            i += 1
            index = bits * i
            curr_cell = cell[prev_index:index]

            b_number = ''.join(str(x) for x in curr_cell)

            # Only for debbuging
            # print b_number

            if base64:
                cypher_text += BaseSF.get_char(b_number)
            else:
                cypher_text += chr(int(b_number, 2))

            prev_index = index

        return cypher_text

    def GetArrayDecoded(self, text, seed, base64=False):
        return self.GetArrayEncoded(text, seed, base64=base64)

    def GetStringDecoded(self, cell, base64=False):
        return self.GetStringEncoded(cell, base64=base64)

    def OneTimePad_encode(self, text, seed, base64=False):
        cell = self.GetArrayEncoded(text, seed, base64=base64)
        string = self.GetStringEncoded(cell, base64=base64)

        return string

    def OneTimePad_decode(self, text, seed, base64=False):
        return self.OneTimePad_encode(text, seed, base64=base64)


encoder = TextCypher()

seed = [0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0]
seed2 = [0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0]

coded = encoder.OneTimePad_encode("SENDMONEY", seed, base64=True)
decoded = encoder.OneTimePad_decode(coded, seed2, base64=True)

print "Coded: %s" % coded
print "Decoded: {}".format(decoded)
